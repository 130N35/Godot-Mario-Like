extends CharacterBody2D

@export var move_speed = 25
@export var gravity = 700

var flipendo = false
var direction = 1
var dead = false
var start = false
var right_angle: Node2D

func _ready():
	right_angle = get_node("/root/Scene/Camera2D/RightAngle")

func _process(delta):
	if !dead && self.global_position.x <= self.right_angle.global_position.x:
		direction = 1 if flipendo else -1
		var collide = self.move_and_collide(Vector2(move_speed * delta * direction, 0))
		
		if collide != null:
			flipendo = !flipendo

		$AnimatedSprite2D.play("walk")
		$AnimatedSprite2D.speed_scale = 0.5
	
	velocity.y += gravity * delta
	move_and_slide()

func kill():
	if dead:
		return
	
	$AnimatedSprite2D.play("death")
	self.set_collision_mask_value(1, false)
	self.set_collision_layer_value(2, false)
	dead = true
	
	velocity += Vector2(60, -150)
	await get_tree().create_timer(2).timeout
	queue_free()
