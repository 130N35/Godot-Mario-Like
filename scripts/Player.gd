extends CharacterBody2D

@export var low_gravity = 300
@export var high_gravity = 700
@export var walk_speed = 100
@export var sprint_speed = 200 
@export var jump_velocity = 150

@export var inertia = 200
@export var air_control_inertia = 400

@export var sprite: AnimatedSprite2D

@export var WorldMusicPlayer: AudioStreamPlayer
@export var jump_sound: AudioStream
@export var die_sound: AudioStream

var toggle_gravity = true
var actual_gravity = low_gravity
var is_running = false
var dead = false

func kill():
	if dead:
		return

	WorldMusicPlayer.stream = die_sound
	WorldMusicPlayer.autoplay = false
	WorldMusicPlayer.play()

	sprite.play("Death")
	self.set_collision_mask_value(1, false)
	self.set_collision_mask_value(3, false)
	$EnemyCollisionBody.set_collision_layer_value(2, false)
	$EnemyCollisionBody.set_collision_mask_value(2, false)
	dead = true

	toggle_gravity = false
	velocity.y = 0
	velocity.x = 0

	await get_tree().create_timer(0.5).timeout
	toggle_gravity = true

	velocity.y += -300

	await WorldMusicPlayer.finished
	get_tree().reload_current_scene()

func jump():
	$MarioSound.stream = jump_sound
	$MarioSound.play()
	velocity.y = -jump_velocity

func _physics_process(delta):
	if !dead:
		is_running = Input.is_action_pressed("Sprint")
		var speed = sprint_speed if is_running else walk_speed

		if is_running:
			sprite.speed_scale = 2

		var is_grounded = is_on_floor()
		var direction = Input.get_axis("Left", "Right") * speed
		var state = "Idle"

		if not is_grounded:
			state = "Jump"
		elif velocity.x != 0:
			state = "Walk"

		if self.global_position.y > 6:
			kill()
			return

		sprite.play(state)

		if direction != 0:
			sprite.flip_h = velocity.x < 0

		if Input.is_action_pressed("Jump") and velocity.y < 0:
			actual_gravity = low_gravity
		else:
			actual_gravity = high_gravity

		var coef = (inertia if is_grounded else air_control_inertia) * delta
		
		velocity.x = (velocity.x * coef + direction) / (1 + coef)

		if Input.is_action_just_pressed("Jump") and is_grounded:
			jump()

	velocity.y += actual_gravity * delta * int(toggle_gravity)
	move_and_slide()
