extends RigidBody2D

@export var player_controller: CharacterBody2D

func _ready():
	self.body_entered.connect(on_collision)

func on_collision(body: CharacterBody2D):
	var hit = body.global_position.y - self.global_position.y

	if hit > 2:
		player_controller.jump()
		body.kill()
	else:
		player_controller.kill()
